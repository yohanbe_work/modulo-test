<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$context = Timber::context();
$context['posts'] = new Timber\PostQuery();
if ( is_post_type_archive( $post_types = 'evenements' )){
	$juin = '202006';
	$juin_args = array(
		'post_type' => 'evenements',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
			  'key' => 'event_start_date',
			  'compare' => 'LIKE',
			  'value' => $juin
			)
		  ),
		'orderby' => 'event_start_date',
		'order' => 'ASC'
	);
	$context['evenements_juin'] = Timber::get_posts($juin_args);

	$juillet = '202007';
	$juillet_args = array(
		'post_type' => 'evenements',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
			  'key' => 'event_start_date',
			  'compare' => 'LIKE',
			  'value' => $juillet
			)
		  ),
		'orderby' => 'event_start_date',
		'order' => 'ASC'
	);
	$context['evenements_juillet'] = Timber::get_posts($juillet_args);

	$aout = '202008';
	$aout_args = array(
		'post_type' => 'evenements',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
			  'key' => 'event_start_date',
			  'compare' => 'LIKE',
			  'value' => $aout
			)
		  ),
		'orderby' => 'event_start_date',
		'order' => 'ASC',
	);
	$context['evenements_aout'] = Timber::get_posts($aout_args);
}

$templates = array( 'archive.twig' );
Timber::render( $templates, $context );