<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::context();
$context['post'] = Timber::query_post();
$context['event_start_date'] = get_field('event_start_date');
$context['event_last_date'] = get_field('event_last_date');
$args = array(
	'post_type' => 'evenements',
	'posts_per_page' => -1,
	'order' => 'DESC'
);
$context['evenements'] = Timber::get_posts($args);

Timber::render( array( 'single-evenements.twig' ), $context );
?>
