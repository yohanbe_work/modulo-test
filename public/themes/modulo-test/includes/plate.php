<?php 

declare(strict_types=1);

add_theme_support('plate-disable-menu', [
    'edit.php', // articles
    'edit-comments.php', // comments
    'index.php', // dashboard
//  'upload.php', // media
]);

add_theme_support('plate-disable-editor', [
    'commentsdiv',
    'commentstatusdiv',
    'linkadvanceddiv',
    'linktargetdiv',
    'linkxfndiv',
    'postcustom',
    'postexcerpt',
    'revisionsdiv',
    'slugdiv',
    'sqpt-meta-tags',
    'trackbacksdiv',
    //'categorydiv',
    //'tagsdiv-post_tag',
]);

add_theme_support('plate-disable-dashboard', [
    'dashboard_activity',
    'dashboard_incoming_links',
    'dashboard_plugins',
    'dashboard_recent_comments',
    'dashboard_primary',
    'dashboard_quick_press',
    'dashboard_recent_drafts',
    'dashboard_secondary',
    //'dashboard_right_now',
]);

add_theme_support('plate-disable-toolbar', [
    'archive',
    'comments',
    'wp-logo',
    'edit',
    'appearance',
    'view',
    'new-content',
    'updates',
    'search',
]);

// Disable dashboard tabs.
add_theme_support('plate-disable-tabs', ['help']);

// Set custom permalink structure.
// add_theme_support('plate-permalink', '/%category%/');

// Set custom footer text.
// add_theme_support('plate-footer-text', 'Thank you for creating with <a href="https://wordplate.github.io">WordPlate</a>.');
