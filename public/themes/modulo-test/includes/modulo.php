<?php

function modulo() {
    //  wp_deregister_script('jquery');
        wp_enqueue_style('app-css', get_template_directory_uri() . '/assets/styles/app.css');
        wp_enqueue_script('app-js', get_template_directory_uri() . '/assets/scripts/app.js', '', '', true);
    }
add_action('wp_enqueue_scripts', 'modulo');

function modulo_cpt() {
	// On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
	$labels = array(
		'name'                => __( 'Événements', 'Post Type General Name'),
		'singular_name'       => __( 'Événement', 'Post Type Singular Name'),
		'menu_name'           => __( 'Événements'),
		'all_items'           => __( 'Toutes les événements'),
		'view_item'           => __( 'Voir les événements'),
		'add_new_item'        => __( 'Ajouter un nouvel événement'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer l\'événement'),
		'update_item'         => __( 'Modifier l\'événement'),
		'search_items'        => __( 'Rechercher un événement'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	// On peut définir ici d'autres options pour notre custom post type
	$args = array(
		'label'               => __( 'Les événements'),
		'description'         => __( 'Liste des événements'),
		'labels'              => $labels,
		'menu_icon'           => 'dashicons-admin-post',
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		// Différentes options supplémentaires
        'show_in_rest'        => true,
		'hierarchical'        => false,
		'public'              => true,
        'has_archive'         => true,
        'rewrite'			  => array( 'slug' => 'evenements'),
        'taxonomies'          => array( 'category' ),
	);
	
	// On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
	register_post_type( 'evenements', $args );
}
add_action( 'init', 'modulo_cpt', 0 );

function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;
    return array(
     'index.php',
     'edit.php?post_type=evenements',
     'edit.php?post_type=page',
     'edit.php',
 );
}
add_filter( 'custom_menu_order', 'custom_menu_order' );
add_filter( 'menu_order', 'custom_menu_order' );
