const mix = require('laravel-mix');
const LiveReloadPlugin = require('webpack-livereload-plugin');
require('dotenv').config();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your WordPlate application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JavaScript files.
 |
 */

const theme = process.env.WP_THEME;

mix.setResourceRoot('../');
mix.setPublicPath(`public/themes/${theme}/assets`);
mix.copyDirectory('resources/images', 'public/themes/modulo-test/assets/images');
// mix.react('resources/scripts/react-app.js', 'scripts');
mix.js('resources/scripts/app.js', 'scripts');
mix.sass('resources/styles/app.scss', 'styles');
mix.options({
    postCss: [
        require('cssnano')({
        discardComments: {
            removeAll: true,
        },
        }),
        require('postcss-unprefix'),
        require('autoprefixer'),
    ]
});
mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery']
});
mix.browserSync({
    proxy: 'modulo-test'
});
mix.webpackConfig({
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            loader: "babel-loader"
            }]
        },
    plugins: [new LiveReloadPlugin()]
});
mix.version();
